d3.select(".table-wrapper-scroll-y").on("wheel", function(d,e,i)
{
    deltaY = d3.event.deltaY;
    if (deltaY > 0){
        deltaY = 10;
    } else {
        deltaY = -10;
    }

    tableWrapperHeight = d3.select(".table-wrapper-scroll-y").style("height");
    tableWrapperHeight = tableWrapperHeight.substring(0, tableWrapperHeight.length - 2);
    tableWrapperHeight = parseInt(tableWrapperHeight)+parseInt(deltaY);

    inciTextHeight = d3.select("#inciText").style("height");
    inciTextHeight = inciTextHeight.substring(0, inciTextHeight.length - 2);
    inciTextHeight = parseInt(inciTextHeight)-parseInt(deltaY);

    d3.select("#inciText").style("height",inciTextHeight+"px") ;
    d3.select(".table-wrapper-scroll-y").style("height",tableWrapperHeight+"px") ;
   
});

function countWords(string){
    var regEx = /\s*,\s+/g;
    var listeInci = string.split(regEx);

    countedWords = [];

    
    listeInci.forEach(inci => {

        if (countedWords[inci] > 0)
        {
            countedWords[inci] += 1;
        } 
        else 
        {
            countedWords[inci] = 1;
        }

    });

    //Recupere les entrees sous forme de tableau
    countedWords = Object.entries(countedWords);

    //Ordonne sous forme décroissant
    sort = countedWords.sort(function(a, b){return b[1]-a[1]});

    return sort;
}
function showInci()
{
    var inci = document.getElementById("inciText").value;

    //Suprimer espace de debut
    //Suprimer virgule de fin

    inciToRequest(inci);
    //countWord(listeInci);
}

function inciToRequest(inci)
{
    //Remplacer les virgules
    var regEx = /\s*,\s+/g;
    var listeInci = inci.replace(regEx, "','")
    listeInci = "'" + listeInci + "'";
    

    var request = "SELECT * FROM inci where INCI_name in (" + listeInci +")";
    request += " order by Function;";
    
    document.cookie = "request=" + request + ";";
    document.cookie = "requestAffiche=json;";
    callDB();
}

function callDB()
{
    $.ajax({
        url: "db.php",
        context: document.getElementById('tableInci'),
        dataType: "json"
      }).done(function(json) {

            //Done
            listHead = ["#","INCI_name","Function"];
            listData = json;
            toCount = "";
            
            
            updateHead = d3.select("#tableInci thead tr").selectAll("th").data(listHead);
            updateBody = d3.select("#tableInci tbody").selectAll("tr").data(listData);


            updateHead.exit();
            updateBody.exit();
            

            //Create head
            thHead = updateHead
            .enter()
            .append("th")
            .html(function(head){return head});


            //Create body
            trBody = updateBody
            .enter()
            .append("tr");

            trBody
            .append("th")
            .html(function(data,i){return i+1});

            trBody
            .append("td")
            .html(function(inci){return inci['INCI_name']});

            trBody
            .append("td")
            .html(function(inci){
                toCount += inci['Function'] + ", ";
                return inci['Function']
            });


            
 
            //Update
            thHead
                .merge(updateHead);
            trBody
                .merge(updateBody);

                
            listData = countWords(toCount);
            listData.pop();

            if (listData.length > 0)
            {
                inciOrdered = listData.map(function(inci)
                {
                    inciConcat = inci[0]+' ('+inci[1]+')';
                    return inciConcat;
                });
                d3.select("#showWords").html(inciOrdered);


                heightConst = 20;
                updateGraphXY = d3.select("#graphXY").selectAll("div").data(listData);
                updateGraphXY.exit();
                updateGraphXY
                .enter()
                .append("div")
                .attr("class","bar bg-primary text-light mr-1")
                .style("height",function(inci){return inci[1]*heightConst+"px";})
                .html(function(inci){return inci[0];})
                .merge(updateGraphXY);
            }
            
            



      });
}
