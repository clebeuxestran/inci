<html>

    <?php session_start(); ?>

    <head>
    <?php 
        echo $_SESSION["head"];

    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['root'] ?>/src/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['root'] ?>/src/css/my.css">
    
    </head>


    <body >
    <div class="">
    <nav class="text-light text-center text-uppercase">Alex</nav>
    <br>
    <div class="inci container">
        
            <div class="row mb-2">
                <textarea class="col-md-12" id="inciText" rows="10">Hippophae Rhamnoides Water, Glycerin, Butylene Glycol, Caprylic/Capric Triglyceride, Betaine, Helianthus Annuus (Sunflower) Seed Oil, Cetearyl Alcohol, Cetearyl Olivate, Sorbitan Olivate, Macadamia Ternifolia Seed Oil, 1,2-Hexanediol, Sodium Hyaluronate, Arginine, Panthenol, Dimethicone, Ethylhexylglycerin, Carbomer, Allantoin, Xanthan Gum</textarea>

            </div>
            <div class="row d-flex justify-content-end">
            
              <input class="col11 btn btn-primary  ml-2" type="button" id="showStats" value="Stats" onclick="showInci()">
              <input class="col12 btn btn-primary  ml-2" type="button" id="showInci" value="Show" onclick="showInci()">
              
                
                
            </div>
            <div class="row d-flex align-items-end">
              <div class="col-7 d-flex align-items-end" id="graphXY"></div>
              <i class="col-5" id="showWords"></i>
            </div>
            

      <div class="table-responsive table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-light mt-4" id="tableInci">
          <thead class="thead-dark"><tr></tr></thead>
          <tbody></tbody>
        </table> 
      </div>

      

      

    </div>
    

    </body>

    

    <footer>
        <?php require "footer.php" ?>
        <script src="<?php echo $_SESSION['root'] ?>/src/js/d3.min.js"></script>
        <script src="<?php echo $_SESSION['root'] ?>/src/js/jquery.min.js"></script>
        <script src="<?php echo $_SESSION['root'] ?>/src/js/bootstrap.min.js"></script>
        <script src="<?php echo $_SESSION['root'] ?>/src/js/my.js"></script>
    </footer>
    
</html>
